import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-services-catalog',
  templateUrl: './services-catalog.page.html',
  styleUrls: ['./services-catalog.page.scss'],
})
export class ServicesCatalogPage implements OnInit {

  service: any;
  findText = '';

  constructor(private route: ActivatedRoute, private serv: ServicesService) { }

  ngOnInit() {
    this.getServices();
  }

  getServices(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.serv.getAll('tipoEquipo/'+id+'/catalogosServicio').then((val) => {
      this.service = val.data;
      console.log(this.service);
    });
  }

    find( event ) {
      // console.log(event);
      this.findText = event.detail.value;
    }

}
 