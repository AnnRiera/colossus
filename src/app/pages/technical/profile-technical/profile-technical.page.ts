import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { User } from 'src/app/clases/user'
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-profile-technical',
  templateUrl: './profile-technical.page.html',
  styleUrls: ['./profile-technical.page.scss'],
})
export class ProfileTechnicalPage implements OnInit {

  usuario: User= new User();
  especialidad: any;
  skills: any[] = [];

  constructor(private storage: Storage, private service: ServicesService) { }

  ngOnInit() {
    this.storage.get('tecnico').then((val) => {
      this.usuario=JSON.parse(val);
      this.findSex();
      this.getSkills();
    });
    
    }

    getSkills(){
      this.service.getAll('empleado/'+this.usuario.id+'/especialidad').then((val) => {
        this.skills = val.data;
      });
    }

  findSex()
  {
    if (this.usuario.sexo=="M"){
    this.usuario.sexo= "Hombre"; }
    else {
      this.usuario.sexo= "Mujer"; 
    }
  }

}
