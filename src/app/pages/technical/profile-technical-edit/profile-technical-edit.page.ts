import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Storage } from '@ionic/storage';
import {User} from 'src/app/clases/user'
import { FormControl, FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ServicesService } from 'src/app/services/services.service';
import { ActionSheetController } from '@ionic/angular';

declare var window: any;

@Component({
  selector: 'app-profile-technical-edit',
  templateUrl: './profile-technical-edit.page.html',
  styleUrls: ['./profile-technical-edit.page.scss'],
})
export class ProfileTechnicalEditPage implements OnInit {

  image: string;
  usuario: User= new User();
  capturedSnapURL:string;
  editTechnicalGroup: FormGroup;
  skill: FormArray;
  check: any [] = [];
  spec: any[] = [];

  constructor(private camera: Camera, public storage: Storage, private formBuilder: FormBuilder, private toastCtrl: ToastController, private router: Router, private service: ServicesService,
              private actionSheetController: ActionSheetController) { 
  }

  ngOnInit() {
    this.storage.get('tecnico').then((val) => {
      this.usuario=JSON.parse(val);
      this.getSkills();
    });

    this.editTechnicalGroup = this.formBuilder.group({
      email: new FormControl('', (Validators.required, Validators.email)),
      address: new FormControl('', Validators.required),
      phone: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      skills: new FormArray([], Validators.required)
    });
  }

  get specialties()
  {
    return this.editTechnicalGroup.get('skills') as FormArray;
  }

  //Método para almacenar las especialidades seleccionadas por el técnico.
  onChange(event){
    for (var i=0; i<event.detail.value.length; i++){
      this.check.push(event.detail.value[i]);
      this.skill = this.editTechnicalGroup.get('skills') as FormArray;
      this.skill.push(
        this.formBuilder.control(this.check[i])
      );
    }
  }

  getSkills(){
    this.service.getAll('empleado/'+this.usuario.id+'/especialidad').then((val) => {
      this.spec = val.data;
    });
  }

  async onSubmit() 
  {
    console.log('FormGroup general: ',this.editTechnicalGroup.value);
    console.log('Correo: ',this.editTechnicalGroup.get('email').value);
    console.log('Dirección: ', this.editTechnicalGroup.get('address').value);
    console.log('Teléfono: ', this.editTechnicalGroup.get('phone').value);
    console.log('Selección de especialidades: ', this.editTechnicalGroup.get('skills').value);

    if (this.editTechnicalGroup.valid)
    {
      let toast = await this.toastCtrl.create({
        message: `Se han cambiado exitosamente sus datos.`,
        duration: 6000
      });

      this.router.navigate(['/profile-technical']);
      toast.present();
    }
    else {
      console.log('Compruebe que todos los campos estén correctos.');
    }
    this.editTechnicalGroup.reset();
  }
  camara() {

    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.CAMERA
    };

    this.procesarImagen( options );

  }

  libreria() {

    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    };

    this.procesarImagen( options );

  }


  procesarImagen( options: CameraOptions ) {

    this.camera.getPicture(options).then( ( imageData ) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):

     //  const img = window.Ionic.WebView.convertFileSrc( imageData );
//console.log(img)
     // this.postsService.subirImagen( imageData );
    //  this.tempImages.push( img );
    this.image=`data:image/jpeg;base64,${imageData}`;

     }, (err) => {
      // Handle error
     });
  } 
  async openActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Foto de perfil',
      buttons: [{
        text: 'Desde la cámara',
        handler: () => {
          console.log('¡Clickeaste cámara!');
          this.camara();
        }
      }, {
        text: 'Desde la galería',
        handler: () => {
          console.log('¡Clickeaste galería!');
          this.libreria();
        }
      }, {
        text: 'Eliminar foto',
        role: 'destructive',
        handler: () => {
          console.log('¡Clickeaste eliminar!');
        }
      }, {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('¡Clickeaste cancelar!');
        }
      }]
    });
    await actionSheet.present();
  }

}
