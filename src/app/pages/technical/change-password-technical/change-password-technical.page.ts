import { Component, OnInit } from '@angular/core';
/* Hay que importar desde la librería de Angular el Validators (que es para las validaciones nativas del RF),
  el FormControl, el FormBuilder es para un uso opcional.*/
import { Validators, FormGroup, FormControl, FormBuilder} from '@angular/forms';
import { ServicesService } from 'src/app/services/services.service';
import { ToastController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password-technical',
  templateUrl: './change-password-technical.page.html',
  styleUrls: ['./change-password-technical.page.scss'],
})
export class ChangePasswordTechnicalPage implements OnInit {

  //Se declara el formGroup.
  changePassGroup: FormGroup;

  constructor(private formBuilder: FormBuilder, private services: ServicesService,private alertCtrl: AlertController, private toastCtrl: ToastController, private router: Router) {
  /*En este caso se usa el formBuilder.group para crear el FormGroup, esto es indistinto, se puede usar el
      FormBuilder o directamente el FormGroup, fue por hacerlo de dos maneras distintas.*/
    this.changePassGroup = this.formBuilder.group({
    npass : new FormControl('', Validators.required),
    cnpass : new FormControl('', Validators.required)
  })
  }

  ngOnInit() {
  }

  async onSubmit(){
     /* Se usa el valid del formGroup, que también retorna un booleano, en este caso hay que comprobar
        el valid de cada formControl, porque si alguno de los dos es false, todo el formGroup es false.
        Con el get().value se obtiene el valor o el contenido de cada formControl, por eso se le pasa
        el nombre de cada formControl, para acceder a lo que contiene cada uno. */
    if (this.changePassGroup.valid && (this.changePassGroup.get('npass').value === this.changePassGroup.get('cnpass').value)) {
      console.log('Contraseña nueva: ',this.changePassGroup.get('npass').value);
      console.log('Repetición de contraseña nueva: ',this.changePassGroup.get('cnpass').value);   
      let toast = await this.toastCtrl.create({
       message: 'La contraseña ha sido cambiada exitosamente.',
       duration: 5000,
       color: 'dark',
       showCloseButton: true,
       closeButtonText: 'X'
     });
     await toast.present();
     setTimeout(() => {
       this.router.navigate(['/profile-technical-edit']);
     });
     this.changePassGroup.reset();
    }
    else {
      console.log('Las contraseñas no son válidas, o no son iguales');
      let toast = await this.toastCtrl.create({
        message: 'Compruebe que las contraseñas sean válidas o sean iguales o que los campos estén llenos ',
        duration: 5000,
        color: 'danger',
        showCloseButton: true,
        closeButtonText: 'X'
      });
     await toast.present();
    }
    this.changePassGroup.reset();
 }
}
