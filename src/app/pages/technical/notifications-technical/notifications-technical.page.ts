import { Component, OnInit, ViewChild } from '@angular/core';
import { IonList } from '@ionic/angular';

@Component({
  selector: 'app-notifications-technical',
  templateUrl: './notifications-technical.page.html',
  styleUrls: ['./notifications-technical.page.scss'],
})
export class NotificationsTechnicalPage implements OnInit {
  @ViewChild('notification') notification: IonList;
  idprueba='';
  i='';
 
  public notifications: any  = [
 
    {
      notification: 
        {
          name:'Nueva asignación',
          email: '¡Has sido asignado para comenzar una reparación!',
          id: 1
        }
    },

    {
      notification: 
        {
          name:'Nueva asignación',
          email: '¡Has sido asignado para comenzar una reparación!',
          id: 2
        }   
    }, 
    {
      notification: 
        {
         name:'Nueva asignación',
         email: '¡Has sido asignado para comenzar una reparación!',
         id: 3
        }
    }
 
];
  constructor() { }

  ngOnInit() {
  }
  
  borrar(id) {
    
    
    
    var pos;
    console.log(this.notifications.length);
    for(let j=0; j<this.notifications.length; j++) {
      if(this.notifications[j].notification.id === id) {
        pos = j;
      } //guardo la posicion del objeto dentro del arreglo que tiene el id igual al id que envio como parametro
    } 
    this.notifications.splice(pos,1); //el primer parametro indica la posicion en donde empezaré a eliminar, y el segundo indica cuantos eliminaré

   console.log("la notificación con el id " + id + " ha sido eliminada posicion del arreglo: " +pos);


}
  
}
