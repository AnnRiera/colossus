import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RateService } from 'src/app/services/rate.service';
import { Rate } from 'src/app/clases/rate';

@Component({
  selector: 'app-rate-details',
  templateUrl: './rate-details.page.html',
  styleUrls: ['./rate-details.page.scss'],
})
export class RateDetailsPage implements OnInit {

  @Input() rateServ: Rate;
  rateservice: Rate[];

  constructor(private rates: RateService ,private route: ActivatedRoute) {}

  ngOnInit() {
    this.getRDetail();
  }

  getRDetail(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.rates.getRateDetail(id)
        .subscribe( rate => {
          console.log( rate );
          this.rateServ= rate;
      });
  }

}