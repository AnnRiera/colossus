import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {
  FormBuilder,
  Validators,
  FormControl,
  FormGroup,
  FormArray
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Rate } from "src/app/clases/rate";
import { RateService } from "src/app/services/rate.service";
import { AlertController, ToastController } from "@ionic/angular";

@Component({
  selector: "app-rate-service",
  templateUrl: "./rate-service.page.html",
  styleUrls: ["./rate-service.page.scss"]
})
export class RateServicePage implements OnInit {
  @Input() ratserv: Rate;

  @Output() rattingChange: EventEmitter<number> = new EventEmitter();

  rateServiceGroup: FormGroup;
  item: FormArray;

  rating: any[] = [
    {
      tittle: "¿Qué opina del tiempo que tomó su servicio?"
    },
    {
      tittle: "¿Le pareció fácil hacerle seguimiento a su solicitud?"
    }
  ];

  constructor(
    public toastCtrl: ToastController,
    private rs: RateService,
    private route: ActivatedRoute,
    public alertCtrl: AlertController,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.getRatDetail();

    this.rateServiceGroup = this.formBuilder.group({
      stars: new FormControl("", Validators.required),
      comments: new FormControl("", [
        Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ ¡¿!?,.;]+$")
      ]),
      items: this.formBuilder.array([])
    });

    for (var i = 0; i < this.rating.length; i++) {
      this.item = this.rateServiceGroup.get("items") as FormArray;
      this.item.push(
        this.formBuilder.group({
          question: this.rating[i].tittle,
          value: new FormControl("", Validators.required)
        })
      );
    }
    console.log(this.item);
  }

  get items() {
    return this.rateServiceGroup.get("items") as FormArray;
  }

  async presentContact() {
    console.log("FormGroup general: ", this.rateServiceGroup.value);
    console.log("Estrellas: ", this.rateServiceGroup.get("stars").value);
    console.log("Comentarios: ", this.rateServiceGroup.get("comments").value);

    for (var i = 0; i < this.rating.length; i++) {
      let array = this.rateServiceGroup.controls.items as FormArray;
      let group = array.at(i) as FormGroup;

      console.log("Pregunta: ", group.controls["question"].value);
      console.log("Respuesta - valor:", group.controls["value"].value);
    }

    if (this.rateServiceGroup.valid) {
      const alert = await this.alertCtrl.create({
        header: "Alerta",
        message: "¿Está seguro de calificar nuestro servicio?",
        buttons: [
          {
            text: "Cancelar",

            cssClass: "secondary",
            handler: () => {
              //this.router.navigate(['/rate']);
            }
          },
          {
            text: "Aceptar",
            cssClass: "secondary",
            handler: () => {
              this.alertMessage();
            }
          }
        ]
      });
      await alert.present();
    } else {
      console.log("Compruebe que todos los campos estén correctos.");
      let toast = await this.toastCtrl.create({
        message:
          "Compruebe que todos los campos estén correctos o estén completos.",
        duration: 6000,
        color: "danger",
        showCloseButton: true,
        closeButtonText: "X"
      });
      await toast.present();
    }
    this.rateServiceGroup.reset();
  }

  async alertMessage() {
    let toast = await this.toastCtrl.create({
      message: "Gracias por calificar nuestro servicio",
      duration: 6000,
      color: "dark",
      showCloseButton: true,
      closeButtonText: "X"
    });
    await toast.present();
    this.router.navigate(["/rate"]);
  }

  getRatDetail() {
    const id = +this.route.snapshot.paramMap.get("id");
    this.rs.getRateDetail(id).subscribe(rs => {
      console.log(rs);
      this.ratserv = rs;
    });
  }

  onSubmit() {}
}
