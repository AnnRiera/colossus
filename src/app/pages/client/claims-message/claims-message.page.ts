import { Component, OnInit } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { ClaimsService } from 'src/app/services/claims.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Guarantee } from 'src/app/clases/guarantee';
import { GuaranteeService } from 'src/app/services/guarantee.service';
import { Term } from 'src/app/clases/term';
import { TermService } from 'src/app/services/term.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-claims-message',
  templateUrl: './claims-message.page.html',
  styleUrls: ['./claims-message.page.scss'],
})
export class ClaimsMessagePage implements OnInit {

  
  gua: Guarantee;
  ter: Term;

  guarantee: Guarantee[];
  term: Term[];

  reclamo: string;
  option: any[] = [
    
          
    {
       "id": 1,
       "opcion": " Falla persistente",
       "date" : "17 Jun 2019 08:00"
   
    },
      {
        "id": 2,
       "opcion": " Falla postservicio ",
       "date" : "18 Jun 2019 15:00"
      
    },
    {
      "id": 3,
     "opcion": " lteración/intercambio de componentes/accesorios no justificado ",
     "date" : "18 Jun 2019 15:00"
    
  }

  
     

];

serviceGroup: FormGroup;
nombreOpcion = "";
mensaje = "";
  usuario = {
    reclamo: '',

  };
   
    constructor(public toastCtrl: ToastController,public claimsmessageCtrl: AlertController, private formBuilder: FormBuilder, private router: Router,  private guaS: GuaranteeService , private route: ActivatedRoute, private terS: TermService) { } 
   
 
  ngOnInit() {
    this.getGuaranteeDetail();
    this.getTermDetail();
    this.serviceGroup = this.formBuilder.group({
  
      option: ['', Validators.required],
     reclamo: ['', Validators.required]
     });
     
     
  }



  getGuaranteeDetail(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.guaS.getGuaranteeDetail(id)
        .subscribe( guaS => {
          console.log( guaS );
          this.gua= guaS;
      });
    }
      getTermDetail(){
        const id = +this.route.snapshot.paramMap.get('id');
        this.terS.getTermDetail(id)
            .subscribe( terS => {
              console.log( terS );
              this.ter= terS;
          });
        }
  generarLink() { 
   
 
    this.nombreOpcion = this.serviceGroup.value.option.opcion;
    this.mensaje = this.serviceGroup.value.reclamo;
    
   
   
  }
  
  onSubmitTemplate() {
    console.log('Form submit');
    console.log('Opcion:', this.nombreOpcion);
    console.log('Reclamo:', this.mensaje);
    


    

   
  }

  async presentContact() {
    const contacto = await this.claimsmessageCtrl.create({
        header: 'Alerta',
        message: '¿Desea generar reclamo?',
        buttons: [
          {
            text: 'Cancelar',
            cssClass: 'secondary',
            handler: () => {
            }
          },
          {
            text: 'Aceptar',
            cssClass: 'secondary',
            handler: () => {
              this.onSubmitTemplate();
              this.alertMessage();
              //this.router.navigate(['/app1/tab-technical/home-technical']);
            }
          }
        ]
      });
    await contacto.present();
  }
  
  async alertMessage() 
  {
  
    let toast = await this.toastCtrl.create({
      header: 'Solicitud enviada',
      message: 'En las próximas horas nos estaremos comunicando con usted',
      duration: 6000,
      color: 'dark',
      showCloseButton: true,
      closeButtonText: 'X',
    });
  await toast.present();
  this.router.navigate(['/guarantee']);
 }


}
