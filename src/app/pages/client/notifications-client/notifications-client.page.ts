import { IonList } from '@ionic/angular';
import { Component, OnInit, ViewChild, ApplicationRef } from '@angular/core';
import { PushService } from 'src/app/services/push.service';
import { OSNotificationPayload } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-notifications-client',
  templateUrl: './notifications-client.page.html',
  styleUrls: ['./notifications-client.page.scss'],
})
export class NotificationsClientPage implements OnInit {
  idprueba='';
  i='';
 mensajes: OSNotificationPayload[]=[];
  public notifications: any  = [
 
    {
      notification: 
        {
          name:'Nuevo presupuesto',
          email: '¡Has recibido un nuevo presupuesto!',
          id: 1
        }
    },

    {
      notification: 
        {
          name:'Nuevo presupuesto',
          email: '¡Has recibido un nuevo presupuesto!',
          id: 2
        }   
    }, 
    {
      notification: 
        {
         name:'Nuevo presupuesto',
         email: '¡Has recibido un nuevo presupuesto!',
         id: 3
        }
    }
 
];
  constructor(public pushService: PushService, private applicationRef: ApplicationRef) { }

  ngOnInit() {
    this.pushService.pushListener.subscribe( noti => {
      this.mensajes.unshift( noti );
     this.applicationRef.tick();
    });
  }

  async ionViewWillEnter() {

    console.log('Will Enter - Cargar mensajes');
   // this.userId = await this.pushService.getUserIdOneSignal();

    this.mensajes = await this.pushService.getMensajes();

  }
  
  borrar(id) {
    
    
    
    var pos;
    console.log(this.notifications.length);
    for(let j=0; j<this.notifications.length; j++) {
      if(this.notifications[j].notification.id === id) {
        pos = j;
      } //guardo la posicion del objeto dentro del arreglo que tiene el id igual al id que envio como parametro
    } 
    this.notifications.splice(pos,1); //el primer parametro indica la posicion en donde empezaré a eliminar, y el segundo indica cuantos eliminaré

   console.log("la notificación con el id " + id + " ha sido eliminada posicion del arreglo: " +pos);


}
doRefresh(event) {
  console.log('Actualizando');

  setTimeout(() => {
    console.log('Actualización completada');
    event.target.complete();
  }, 1500);
}
  
}
