import { Component, OnInit, Input } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Guarantee } from 'src/app/clases/guarantee';
import { Term } from 'src/app/clases/term';
import { GuaranteeService } from 'src/app/services/guarantee.service';
import { TermService } from 'src/app/services/term.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-guarantee-detail',
  templateUrl: './guarantee-detail.page.html',
  styleUrls: ['./guarantee-detail.page.scss'],
})
export class GuaranteeDetailPage implements OnInit {

  @Input() gua: Guarantee;
  ter: Term;
  guarantee: Guarantee[];
  term: Term[];
  constructor( private router: Router, public alertCtrl: AlertController, private guaS: GuaranteeService , private route: ActivatedRoute, private terS: TermService) { }

  ngOnInit() {
    this.getGuaranteeDetail();
    this.getTermDetail();
  }
  getGuaranteeDetail(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.guaS.getGuaranteeDetail(id)
        .subscribe( guaS => {
          console.log( guaS );
          this.gua= guaS;
      });
    }
      getTermDetail(){
        const id = +this.route.snapshot.paramMap.get('id');
        this.terS.getTermDetail(id)
            .subscribe( terS => {
              console.log( terS );
              this.ter= terS;
          });
  }
  

}
