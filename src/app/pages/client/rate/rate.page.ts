import { Component, OnInit } from '@angular/core';
import { RateService } from 'src/app/services/rate.service';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.page.html',
  styleUrls: ['./rate.page.scss'],
})
export class RatePage implements OnInit {

  rate: any[] = [];
  findText = '';
  constructor(private rates: RateService) { }

  ngOnInit() {
    this.getRate();
  }

  getRate(){
    this.rates.getRate()
        .subscribe( rates => {
          console.log( rates );
          this.rate = rates;
      });
  }
  find( event ) {
    // console.log(event);
    this.findText = event.detail.value;
  }
  doRefresh(event) {
    console.log('Actualizando');
  
    setTimeout(() => {
      console.log('Actualización completada');
      event.target.complete();
    }, 1500);
  }


}
 