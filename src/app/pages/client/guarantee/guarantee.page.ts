import { Component, OnInit, ViewChild  } from '@angular/core';
import { GuaranteeService } from 'src/app/services/guarantee.service';
import { TermService } from 'src/app/services/term.service';
import { ClaimsService } from 'src/app/services/claims.service';


import { IonSegment } from '@ionic/angular';

@Component({
  selector: 'app-guarantee',
  templateUrl: './guarantee.page.html',
  styleUrls: ['./guarantee.page.scss'],
})
export class GuaranteePage implements OnInit {
  section: string = 'two';
  guarantee: any[] = [];
  term: any[] = [];
  claims: any[] = [];
  findText = '';
  modelo = '';
  descripcion= '';
  @ViewChild(IonSegment) segment: IonSegment;
  constructor(private guara: GuaranteeService, private claim: ClaimsService, private terms: TermService) { }

  ngOnInit() {
    this.section = 'garantia';
    //this.segment.value = 'garantia';
    this.guara.getGuarantee()
    .subscribe( guara => {
      console.log( guara );
      this.guarantee = guara;
    });
    this.claim.getClaims()
    .subscribe( clai => {
      console.log( clai );
      this.claims = clai;
    });
    this.terms.getTerm()
    .subscribe( ter => {
      console.log( ter );
      this.term = ter;
    });
    
  }
  segmentChanged( event ) {

    const segmentValue = event.detail.value;

   if ( segmentValue === 'garantia' ) {
      this.modelo = '';
      this.descripcion = '';

      
      return;
    }

    this.modelo = segmentValue;
    this.descripcion = segmentValue;
   

    console.log(segmentValue);

  }
  segmentButtonClicked(ev: any) {
    console.log('Segment button clicked', ev);
  }
 
  
 
  find( event ) {
    // console.log(event);
    this.findText = event.detail.value;
  }

  doRefresh(event) {
    console.log('Actualizando');
  
    setTimeout(() => {
      console.log('Actualización completada');
      event.target.complete();
    }, 1500);
  }

  

}
