import { Component, OnInit } from '@angular/core';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  contact: any[] = [];
  findText = '';

  constructor( private contacts: ContactService ) {}

  ngOnInit() {
    this.contacts .getContact()
    .subscribe( contacts => {
      console.log( contacts  );
      this.contact = contacts ;
    });
  }

  find( event ) {
    // console.log(event);
    this.findText = event.detail.value;
  }
  doRefresh(event) {
    console.log('Actualizando');
  
    setTimeout(() => {
      console.log('Actualización completada');
      event.target.complete();
    }, 1500);
  }
    
 
}

  
