import { Component, OnInit, Input } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Claims } from 'src/app/clases/claims';
import { ClaimsService } from 'src/app/services/claims.service';
import { Guarantee } from 'src/app/clases/guarantee';
import { GuaranteeService } from 'src/app/services/guarantee.service';
import { Term } from 'src/app/clases/term';
import { TermService } from 'src/app/services/term.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-claims-detail',
  templateUrl: './claims-detail.page.html',
  styleUrls: ['./claims-detail.page.scss'],
})
export class ClaimsDetailPage implements OnInit {

  @Input() gua: Guarantee;
  ter: Term;
  cla: Claims;
  guarantee: Guarantee[];
  term: Term[];
  claims: Claims[];
  constructor( private router: Router, public alertCtrl: AlertController, private guaS: GuaranteeService , private route: ActivatedRoute, private terS: TermService, private claS: ClaimsService) { }

  ngOnInit() {
    this.getGuaranteeDetail();
    this.getTermDetail();
    this.getClaimsDetail();
  }
  getGuaranteeDetail(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.guaS.getGuaranteeDetail(id)
        .subscribe( guaS => {
          console.log( guaS );
          this.gua= guaS;
      });
    }
      getTermDetail(){
        const id = +this.route.snapshot.paramMap.get('id');
        this.terS.getTermDetail(id)
            .subscribe( terS => {
              console.log( terS );
              this.ter= terS;
          });
  }
  getClaimsDetail(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.claS.getClaimsDetail(id)
        .subscribe( claS => {
          console.log( claS );
          this.cla= claS;
      });
}

  


}
