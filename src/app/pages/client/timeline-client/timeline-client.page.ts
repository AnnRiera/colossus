import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesService } from '../../../services/services.service';

@Component({
  selector: 'app-timeline-client',
  templateUrl: './timeline-client.page.html',
  styleUrls: ['./timeline-client.page.scss'],
})
export class TimelineClientPage implements OnInit {

  timeline: any[];
  statusChart:any;
  statusString:any;
  constructor(public services:ServicesService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.services.getAll('ordenServicio/'+id) 
      .then(data => {
        console.log('esto es lo que me traigo', data);
        this.timeline = data.data.agendas;
        this.statusChart = data.data.agendas.estatus;
        this.transformStatus();
        console.log('esto es lo que almaceno', this.timeline);
      }, err => {
        console.log(err);
      });
  }
  transformStatus(){
    if (this.statusChart="E"){
      this.statusString="En espera";
    }
    else if (this.statusChart="L") {
      this.statusString="Completado";
    }
    else {
      this.statusString="Reagendado";
    }
  }
}
