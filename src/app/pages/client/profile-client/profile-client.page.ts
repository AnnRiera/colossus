import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import {User} from 'src/app/clases/user'
@Component({
  selector: 'app-profile-client',
  templateUrl: './profile-client.page.html',
  styleUrls: ['./profile-client.page.scss'],
})
export class ProfileClientPage implements OnInit {
  usuario: User= new User();

  plea = [
    {
      name: 'Tecnología',
      isChecked: true
    },
    {
      name: 'Consolas',
      isChecked: false
    },
    {
      name: 'Libros',
      isChecked: false
    }
  ];

  pref = [
    { 
      name: 'Pepperoni',
      isChecked: true
    },
    { 
      name: 'Sausage',
      isChecked: false
    },
    { 
      name: 'Mushroom',
      isChecked: false
    }
  ];

  constructor(public navCtrl: NavController, private storage: Storage) { }

  ngOnInit() {
    this.storage.get('cliente').then((val) => {
      this.usuario=JSON.parse(val);
      //console.log(this.usuario);
      this.findSex();
    });   
  }
 

  findSex(){
    if (this.usuario.sexo=="M"){
    this.usuario.sexo= "Hombre"; }
    else {
      this.usuario.sexo= "Mujer"; 
    }
  }
  
 /* goTo() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
          usuario: JSON.stringify(this.usuario)
      }
  };
  this.navCtrl.navigateForward(['profile-client-edit'],navigationExtras);
  
  }*/

}
