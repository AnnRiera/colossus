import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Storage } from '@ionic/storage';
import {User} from 'src/app/clases/user'
import { FormControl, FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';

declare var window: any;

@Component({
  selector: 'app-profile-client-edit',
  templateUrl: './profile-client-edit.page.html',
  styleUrls: ['./profile-client-edit.page.scss'],
})
export class ProfileClientEditPage implements OnInit {
  image: string;
  usuario: User= new User();
  capturedSnapURL:string;
  cplea: any [] = [];
  cpref: any [] = [];
  plea = [
    {
      name: 'Tecnología'
    },
    {
      name: 'Consolas'
    },
    {
      name: 'Libros'
    }
  ];

  pref = [
    { 
      name: 'Pepperoni'
    },
    { 
      name: 'Sausage'
    },
    { 
      name: 'Mushroom'
    }
  ];

  clientEditGroup: FormGroup;
  preference: FormArray;
  pleasure: FormArray;
 
  cameraOptions: CameraOptions = {
    quality: 20,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }

  constructor(private camera: Camera,private storage: Storage, private formBuilder: FormBuilder, private toastCtrl: ToastController, private router: Router, private alertCtrl: AlertController,
              private actionSheetController: ActionSheetController) {

    this.clientEditGroup = new FormGroup({
      email: new FormControl('',(Validators.required, Validators.email)),
      address: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      pleasures: new FormArray([], Validators.required),
      preferences: new FormArray([], Validators.required)

    })
    
   }

  ngOnInit() {
   /* this.route.queryParams.subscribe(params => {
     // console.log('Pepperoni', params);
      this.user = JSON.parse(params.usuario);
      console.log(this.user.nombre);
      this.usuarioprueba=this.user.nombre;

  });
*/
this.storage.get('cliente').then((val) => {
  this.usuario=JSON.parse(val);
  //console.log(this.usuario);
});

  }

  onPleasure(event){
    for (var i=0; i<event.detail.value.length; i++){
      this.cplea.push(event.detail.value[i]);
      this.pleasure = this.clientEditGroup.get('pleasures') as FormArray;
      this.pleasure.push(
        this.formBuilder.control(this.cplea[i])
      );
    }
  }

  onPreference(event){
    for (var i=0; i<event.detail.value.length; i++){
      this.cpref.push(event.detail.value[i]);
      this.preference = this.clientEditGroup.get('preferences') as FormArray;
      this.preference.push(
        this.formBuilder.control(this.cpref[i])
      );
    }
  }

  async actualizar(){
    console.log(this.usuario);
    const alert = await this.alertCtrl.create(
      {
        header: 'Alerta',
        message: '¿Está seguro que los datos son correctos?',
        buttons: [
          {
            text: 'Cancelar',
            cssClass: 'secondary',
            handler: () => {
            }
          },
          {
            text: 'Aceptar',
            cssClass: 'secondary',
            handler: () => {
              this.alertSubs();
            }
          }
        ]
      });
    await alert.present();
  }

  async alertSubs() {
      let toast = await this.toastCtrl.create({
        message: 'Actualización exitosa',
        duration: 6000,
        color: 'dark',
        showCloseButton: true,
        closeButtonText: 'X',
      });

      await toast.present();
      this.router.navigate(['/profile-client']);
    
  }
  camara() {

    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.CAMERA
    };

    this.procesarImagen( options );

  }

  libreria() {

    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    };

    this.procesarImagen( options );

  }


  procesarImagen( options: CameraOptions ) {

    this.camera.getPicture(options).then( ( imageData ) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):

     //  const img = window.Ionic.WebView.convertFileSrc( imageData );
//console.log(img)
     // this.postsService.subirImagen( imageData );
    //  this.tempImages.push( img );
  this.image=`data:image/jpeg;base64,${imageData}`;
     }, (err) => {
      // Handle error
     });
  }

  async onSubmit() 
  {
    console.log('FormGroup general: ',this.clientEditGroup.value);
    console.log('Correo: ',this.clientEditGroup.get('email').value);
    console.log('Dirección: ', this.clientEditGroup.get('address').value);
    console.log('Teléfono: ', this.clientEditGroup.get('phone').value);
    console.log('Selección de gustos: ', this.clientEditGroup.get('pleasures').value);
    console.log('Selección de preferencias: ', this.clientEditGroup.get('preferences').value);

    if (this.clientEditGroup.valid)
    {
      let toast = await this.toastCtrl.create({
        message: `Se han cambiado exitosamente sus datos.`,
        duration: 6000
      });

      this.router.navigate(['/profile-client']);
      toast.present();
    }
    else {
      console.log('Compruebe que todos los campos estén correctos.');
    }
  }

  async openActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Foto de perfil',
      buttons: [{
        text: 'Desde la cámara',
        handler: () => {
          console.log('¡Clickeaste cámara!');
          this.camara();
        }
      }, {
        text: 'Desde la galería',
        handler: () => {
          console.log('¡Clickeaste galería!');
          this.libreria();
        }
      }, {
        text: 'Eliminar foto',
        role: 'destructive',
        handler: () => {
          console.log('¡Clickeaste eliminar!');
        }
      }, {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('¡Clickeaste cancelar!');
        }
      }]
    });
    await actionSheet.present();
  }
}
