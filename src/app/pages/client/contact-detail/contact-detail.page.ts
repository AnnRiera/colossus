import { Component, OnInit, Input } from '@angular/core';
import { Contact } from 'src/app/clases/contact';
import { ContactService } from 'src/app/services/contact.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.page.html',
  styleUrls: ['./contact-detail.page.scss'],
})
export class ContactDetailPage implements OnInit {
  @Input() con: Contact;
  contact: Contact[];

  constructor(private conS: ContactService , private route: ActivatedRoute) { }

  ngOnInit() {
    this.getContactDetail();
  }
  getContactDetail(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.conS.getContactDetail(id)
        .subscribe( conS => {
          console.log( conS );
          this.con= conS;
      });
  }

}
