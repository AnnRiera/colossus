import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-modal-info',
  templateUrl: './modal-info.page.html',
  styleUrls: ['./modal-info.page.scss'],
})
export class ModalInfoPage implements OnInit {

  clientEditGroup: FormGroup;
  preference: FormArray;
  pleasure: FormArray;
  brand: FormArray;
  cplea: any [] = [];
  cpref: any [] = [];
  cbran: any [] = [];

  plea = [
    {
      name: 'Tecnología',
      isChecked: true
    },
    {
      name: 'Consolas',
      isChecked: false
    },
    {
      name: 'Libros',
      isChecked: false
    }
  ];

  pref = [
    { 
      name: 'Pepperoni',
      isChecked: true
    },
    { 
      name: 'Sausage',
      isChecked: false
    },
    { 
      name: 'Mushroom',
      isChecked: false
    }
  ];

  marc = [
    {
      name: 'Samsung',
      isChecked: true
    },
    {
      name: 'LG',
      isChecked: false
    },
    {
      name: 'Iphone',
      isChecked: false
    },
  ];


  constructor(private modalCtrl: ModalController, private formBuilder: FormBuilder) { 
    this.clientEditGroup = new FormGroup({
      pleasures: new FormArray([], Validators.required),
      preferences: new FormArray([], Validators.required),
      brands: new FormArray([], Validators.required)

    })
  }

  ngOnInit() {
  }

  salirModal(){
  this.modalCtrl.dismiss({
    gustos: this.clientEditGroup.get('pleasures').value,
    preferencias: this.clientEditGroup.get('preferences').value,
    marcas: this.clientEditGroup.get('brands').value
  });
  }

  onPleasure(event){
    for (var i=0; i<event.detail.value.length; i++){
      this.cplea.push(event.detail.value[i]);
      this.pleasure = this.clientEditGroup.get('pleasures') as FormArray;
      this.pleasure.push(
        this.formBuilder.control(this.cplea[i])
      );
    }
  }

  onPreference(event){
    for (var i=0; i<event.detail.value.length; i++){
      this.cpref.push(event.detail.value[i]);
      this.preference = this.clientEditGroup.get('preferences') as FormArray;
      this.preference.push(
        this.formBuilder.control(this.cpref[i])
      );
    }
  }

  onBrand(event){
    for (var i=0; i<event.detail.value.length; i++){
      this.cbran.push(event.detail.value[i]);
      this.brand = this.clientEditGroup.get('brands') as FormArray;
      this.brand.push(
        this.formBuilder.control(this.cbran[i])
      );
    }
  }

}
