import { Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.page.html',
  styleUrls: ['./faqs.page.scss'],
})
export class FaqsPage implements OnInit {
  faqs: any[] ;
  response:any[];
  automaticClose = false;
  constructor( private http: HttpClient, private services: ServicesService) { 
   /* this.http.get('assets/data/accordion.json').subscribe( res => {
      this.faqs = res['items'];

    this.faqs[0].open = false;
    });*/
  }

  ngOnInit() {
    this.getFaqs();
  }
  getFaqs(){
    this.services.getAll('preguntaFrecuente').then((val) => {
      this.faqs = val.data;
      this.response=val.data[0].respuesta
      console.log(val.data);
      console.log(this.response);
      this.faqs[0].open = false;
    });
  }

  toggleSection(index) {
    this.faqs[index].open = !this.faqs[index].open;

    if (this.automaticClose && this.faqs[index].open){
      this.faqs
      .filter((item,itemIndex) => itemIndex != index)
      .map(item => item.open = false);
    }
  }

  /*toggleItem(index, childIndex){
    this.faqs[index].children[childIndex].open = !this.faqs[index].children[childIndex].open;
  }*/
}
