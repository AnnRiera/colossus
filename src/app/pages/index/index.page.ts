import { Component, OnInit} from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { PopoverComponent } from 'src/app/components/popover/popover.component';
import { IndexService } from 'src/app/services/index.service';
import { Index } from 'src/app/clases/index';
import { IND } from 'src/assets/data/index';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss']
})
export class IndexPage implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  ind: any[] = [];

  constructor(private indexs: IndexService, private popoverController: PopoverController, private router: Router) {
    this.getIndex();
  }
  
  ngOnInit() {
}

  getIndex(){
    this.indexs.getIndex()
        .subscribe((res: any) => {
          this.ind = res;
      });
  }

  async popover(ev) {
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      event: ev,
      translucent: true,
      cssClass: "popover-class"
    });
    return await popover.present();
  }

  goToCatalog(){
    this.router.navigate(['/catalog']);

  }

}

